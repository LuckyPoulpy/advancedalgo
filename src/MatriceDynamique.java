public class MatriceDynamique {
    private Polygone polygone = null;
    private Etat[][] matrice = null;

    /**
     * Constructeur de la classe MatriceDynamique, qui l'initialise selon la méthode décrite dans le rapport
     * @param p un polygone dont on veut déterminer la meilleure triangulation
     */
    public MatriceDynamique(Polygone p)
    {
        this.polygone = p;
        //on initialise la matrice
        int nbPoints = polygone.nombreDeSommets();
        matrice = new Etat[nbPoints][nbPoints-2];
        int i,j;
        for(i=1; i<=nbPoints;i++)
        {
            setEtat(new Etat(0, null), i, 3);
            for(j = 4; j<=nbPoints-i+1; j++)
            {
                Etat e = new Etat(-1, null);
                setEtat(e, i, j);
            }
        }
    }

    /**
     * Getter pour l'attribut polygone
     * @return un Polygone
     */
    public Polygone getPolygone() {
        return polygone;
    }

    /**
     * Setter pour l'attribut polygone
     * @param polygone la nouvelle valeur du polygone
     */
    public void setPolygone(Polygone polygone) {
        this.polygone = polygone;
    }

    /**
     * Getter pour l'attribut matrice
     * @return la valeur de l'attribut matrice
     */
    public Etat[][] getMatrice() {
        return matrice;
    }

    /**
     * Setter pour l'attribut matrice
     * @param matrice la nouvelle valeur de l'attribut matrice
     */
    public void setMatrice(Etat[][] matrice) {
        this.matrice = matrice;
    }

    /**
     * Renvoie la valeur de l'état associé au polynome commençant par le point i et de taille t
     * @param i le point de départ du polygone courant
     * @param t la taille du polynome courant
     * @return un Etat T(i,t)
     */
    public Etat getEtat(int i, int t)
    {
        return this.matrice[i-1][t-3];
    }

    /**
     * Change l'état correspondant au polynome de taille t commençant par i
     * @param e la nouvelle valeur de l'état
     * @param i le numéro du point courant
     * @param t la taille du polygone courant
     */
    public void setEtat(Etat e, int i, int t)
    {
        this.matrice[i-1][t-3] = e;
    }


    /**
     * Affiche les informations sur la matrice dynamique
     * @return une chaine de caractères montrant le contenu de la matrice
     */
    @Override
    public String toString() {
        String c = "";
        int i, j;
        for (i = 1; i < matrice.length; i++) {
            for (j = 3; j <= matrice[i].length; j++) {
                c += "etat à la case (" + i + ", " + j + ") : " + getEtat(i, j).toString()+"\n";
            }
        }
        return c;
    }
}

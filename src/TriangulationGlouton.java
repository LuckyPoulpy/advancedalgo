import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TriangulationGlouton extends Triangulation {

    /**
     * Constructeur de la classe TriangulationGlouton
     * @param polygone un polygone
     */
    public TriangulationGlouton(Polygone polygone) {
        super(polygone);
        this.monPolygone = polygone;
    }

    private Polygone monPolygone;

    /**
     * Ajoute une corde à la triangulation
     * @param corde une corde
     * @throws IOException
     * @throws Exception
     */
    public void ajouterCorde(Corde corde) throws IOException, Exception{
        super.ajouterCorde(corde);
        //On copie la liste des points du polygone
        List<Point> listePointsTemporaire = this.monPolygone.getListePoints_deep_copy();
        int verificationTaille = listePointsTemporaire.size();
        boolean estEntreAetB = listePointsTemporaire.indexOf(corde.getSommetB()) - listePointsTemporaire.indexOf(corde.getSommetA()) < 3;
        Iterator<Point> monIterateur = listePointsTemporaire.iterator();
        //On enleve les points dont on ne va plus se servir (supprimés par la création de la corde)
        while(monIterateur.hasNext()){
            Point p = (Point) monIterateur.next();

            if( estEntreAetB  && p.getNoPt() < corde.getSommetB().getNoPt() && p.getNoPt() > corde.getSommetA().getNoPt()){
//				System.err.println("Suppression de "+p);
                monIterateur.remove();
            }else if( !estEntreAetB  && (p.getNoPt() > corde.getSommetB().getNoPt() || p.getNoPt() < corde.getSommetA().getNoPt())){
//				System.err.println("Suppression de "+p);
                monIterateur.remove();
            }
        }
        
        if(verificationTaille == listePointsTemporaire.size()){
            System.err.println("Impossible de trouver un sommet a supprimer pour cette corde");
            throw new Exception("Problème de boucle infinie");
        }else{
			//System.err.println("Suppression reussie");
        }
        //Et on crée le polygone
        this.monPolygone = new Polygone((LinkedList)listePointsTemporaire);
        //System.err.println("Inserted polygone of size "+polygoneCurseur.size());
    }


    /**
     * Retourne le nombre de sommets du polygone de la triangulation gloutonne
     * @return
     */
    public int getNbSommetsRestants() {
        return monPolygone.nombreDeSommets();
    }

    /**
     * @return la liste des cordes exterieures du polygone
     */
    public List<Corde> getCordesExterieures(){
        List<Corde> listeCordesExistantes = new LinkedList<Corde>();
        for(Corde c: this.monPolygone.getListeCordesPossibles()){
            if( this.monPolygone.getListePoints().indexOf(c.getSommetA()) ==
                    (this.monPolygone.getListePoints().indexOf(c.getSommetB())+2)%this.monPolygone.nombreDeSommets() ||
                    this.monPolygone.getListePoints().indexOf(c.getSommetA())+2 ==
                            this.monPolygone.getListePoints().indexOf(c.getSommetB())
            ){
                listeCordesExistantes.add(c);
            }
        }
        return listeCordesExistantes;
    }
}
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Polygone extends IOException{

    private LinkedList<Point> listePoints = new LinkedList<Point>();
    private LinkedList<Corde> listeCordesPossibles = new LinkedList<Corde>();
    private LinkedList<Corde> listeCotes = new LinkedList<Corde>();

    /**
     * Constructeur de la classe Polygone
     * @param listePoints la liste des points du polygone
     */
    public Polygone(LinkedList<Point> listePoints){
        //On crée la liste de points correspondant au polygone
        this.listePoints = listePoints;
        //Des qu'on a plus de 1 sommet, on trace les cordes formant les cotes du polygone
        if(nombreDeSommets() > 1){
            int i;
            for(i = 1; i < listePoints.size(); i++){
                Corde cote = new Corde(listePoints.get(i-1),listePoints.get(i));
                listeCotes.add(cote);
            }
            Corde dernierCote = new Corde(listePoints.get(listePoints.size()-1),listePoints.get(0));
            listeCotes.add(dernierCote);
        }
        //Des qu'on a plus de trois sommets, on cree la liste de toutes les cordes qu'il est possible de tracer (sans les cotes)
        if(nombreDeSommets() > 3){
            try {
                this.nouvellesCordes();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Génère toutes les cordes depuis chaque point, sans tracer les côtés du polygone
     */
    public void nouvellesCordes() {
        for(int i = 0; i < listePoints.size(); i++){
            for(int j = 0; j < listePoints.size(); j++) {
                if (i != j) {
                    Corde cordeATracer = new Corde(listePoints.get(i), listePoints.get(j));
                    //Si la corde qu'on veut tracer n'est pas déjà dans la liste des cordes possbiles ou la liste des cotes du polygone, on la trace
                    if (!(listeCordesPossibles.contains(cordeATracer)) && !(listeCotes.contains(cordeATracer))) {
                        listeCordesPossibles.add(cordeATracer);
                    }
                }
            }
        }
    }

    /**
     * Retourne la liste des points du polygone
     * @return une liste de points
     */
    public LinkedList<Point> getListePoints() {
        return listePoints;
    }

    /**
     * retourne la liste des cordes qu'il est possible de tracer
     * @return une liste de cordes
     */
    public LinkedList<Corde> getListeCordesPossibles() {
        return listeCordesPossibles;
    }

    /**
     * Retourne la liste des côtés du polygone
     * @return une liste de cordes
     */
    public LinkedList<Corde> getListeCotes() {
        return listeCotes;
    }

    /**
     * Initialise la liste des cotés
     * @param listeCotes une liste de cordes
     */
    public void setListeCotes(LinkedList<Corde> listeCotes) {
        this.listeCotes = listeCotes;
    }

    /**
     * Ajoute un nouveau point au polygone et met à jour les listes des cotes du polygone
     * @param p un point
     */
    public void ajouterPoint(Point p){
        listePoints.add(p);
        listeCotes.removeLast();
        Corde cordeNmoins1N = new Corde(listePoints.get(listePoints.size()-1),p);
        Corde cordeNZero = new Corde(listePoints.get(listePoints.size()-1),listePoints.get(0));
        listeCotes.add(cordeNmoins1N);
        listeCotes.add(cordeNZero);
    }

    /**
     * Permet de créer une deep copy de la liste des points du polygone
     * @return une liste de points
     */
    public List<Point> getListePoints_deep_copy() {
        List<Point> deep_copy = new LinkedList<Point>();
        for(Point p : this.listePoints){
            deep_copy.add(p);
        }
        return deep_copy;
    }

    /**
     * Retourne le nombre de sommets du polygone
     * @return un entier
     */
    public int nombreDeSommets(){
        return listePoints.size();
    }

    public static void main(String[] args){
        Point a = new Point(0,0);
        Point b = new Point(1,0);
        Point c = new Point(1,1);
        Point d = new Point(0,1);
        Point e = new Point(-0.5,0.5);

        LinkedList<Point> listePoints = new LinkedList<Point>();
        listePoints.add(a);
        listePoints.add(b);
        listePoints.add(c);
        listePoints.add(d);

        Polygone polygone1 = new Polygone(listePoints);
        System.out.println(("Liste des cotés du polygone 1"));
        for(int i = 0; i < polygone1.getListeCotes().size(); i++){
            System.out.println(polygone1.getListeCotes().get(i).toString());
        }
        System.out.println(("Liste des cordes du polygone 1"));
        for(int i = 0; i < polygone1.getListeCordesPossibles().size(); i++){
            System.out.println(polygone1.getListeCordesPossibles().get(i).toString());
        }

        listePoints.add(e);

        Polygone polygone2 = new Polygone(listePoints);
        System.out.println(("Liste des cotés du polygone 2"));
        for(int i = 0; i < polygone2.getListeCotes().size(); i++){
            System.out.println(polygone2.getListeCotes().get(i).toString());
        }
        System.out.println(("Liste des cordes du polygone 2"));
        for(int i = 0; i < polygone2.getListeCordesPossibles().size(); i++){
            System.out.println(polygone2.getListeCordesPossibles().get(i).toString());
        }
    }


}

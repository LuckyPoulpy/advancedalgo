import java.util.*;

public class AppliEssaisSuccessifs {
    /**
     * Mise en place de l'algo des essais successifs
     * @param t une triangulation
     * @return un triangulation minimale
     */
    public static Triangulation essaisSuccessifs(Triangulation t) {
        List<Triangulation> listeTriangulations = new LinkedList<Triangulation>();
        Triangulation triangulationTemporaire = null;
        for (Corde corde : t.getCordesPossibles()) {
            //On va tester toutes les cordes possibles du polygone
            //Si cette corde est valide on l'ajoute à la triangulation
            if (t.valideCorde(corde)) {
                triangulationTemporaire = t.creer_deep_copy();
                try {
                    triangulationTemporaire.ajouterCorde(corde);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Et on génénère une nouvelle triangulation avec la nouvelle corde prise en compte
                listeTriangulations.add(essaisSuccessifs(triangulationTemporaire));
            }
        }
        if (triangulationTemporaire == null) {
            if (t.estValide()) {
                //System.out.println("Triangulation trouvée valide avec "+t.getListeCordesUtilisees().size()+" cordes ");
            } else {
                //System.out.println("Triangulation trouvée non valide : "+t.getListeCordesUtilisees().size()+" cordes utilisées au lieu de "+ (t.getPolygone().nombreDeSommets()-3));
            }
            return t;
        } else {
            //A la fin de la récursion, on cherche dans la liste de toutes les triangulations trouvées la triangulation minimale
            Triangulation triangulationMinimale = listeTriangulations.get(0);
            for (Triangulation triangulationTemporaire2 : listeTriangulations) {
                //System.out.println("Triangulation : " + triangulationTemporaire2.toString());
                if ((!triangulationMinimale.estValide()) || (triangulationTemporaire2.estValide() && triangulationTemporaire2.longueur() < triangulationMinimale.longueur())) {
                    triangulationMinimale = triangulationTemporaire2;
                }
            }
            return triangulationMinimale;
        }
    }

    /**
     * Algo des essais successis avec élagage pour trouver la triangulation minimum
     * @param t une tirangulation
     * @param minimumTrouve la longueur minimale trouvée
     * @return
     */
    public static Triangulation essaisSuccessifsElagage(Triangulation t, double minimumTrouve) {
        List<Triangulation> listeTriangulations = new LinkedList<Triangulation>();
        Triangulation triangulationTemporaire = null;
        //Elagages expliqués dans le compte-rendu
        if ((t.getListeCordesUtilisees().size() + t.getCordesPossibles().size()) >= (t.getPolygone().nombreDeSommets() - 3)) {
            for (Corde corde : t.getCordesPossibles()) {
                if (t.valideCorde(corde)) {
                    if (t.longueur() + corde.getLength() < minimumTrouve) {
                        triangulationTemporaire = t.creer_deep_copy();
                        try {
                            triangulationTemporaire.ajouterCorde(corde);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Triangulation triangulationTemporaire2 = essaisSuccessifsElagage(triangulationTemporaire, minimumTrouve);
                        if (triangulationTemporaire2 != null) {
                            triangulationTemporaire = triangulationTemporaire2;
                            if (triangulationTemporaire.longueur() < minimumTrouve) {
                                minimumTrouve = triangulationTemporaire.longueur();
                            }
                            listeTriangulations.add(triangulationTemporaire);
                        }
                    }
                }
            }
        }
        if (triangulationTemporaire == null) {
            if (t.getListeCordesUtilisees().size() == t.getPolygone().nombreDeSommets() - 3) {
                //System.out.println("Triangulation trouvée valide avec "+t.getListeCordesUtilisees().size()+" cordes ");
                return t;
            } else {
                //System.out.println("Triangulation trouvée non valide : "+t.getListeCordesUtilisees().size()+" cordes utilisées au lieu de "+ (t.getPolygone().nombreDeSommets()-3));
                return null;
            }
        } else {
            //On cherche la triangulation minimale des triangulations trouvées
            Triangulation triangulationMinimale = null;
            if (!listeTriangulations.isEmpty()) {
                triangulationMinimale = listeTriangulations.get(0);
                for (Triangulation triangulationTemporaire1 : listeTriangulations) {
                    if (triangulationTemporaire1.longueur() < triangulationMinimale.longueur()) {
                        triangulationMinimale = triangulationTemporaire1;
                    }
                }
            }

            return triangulationMinimale;
        }
    }

    /**
     * Permet de tester les essais successifs
     * @param args
     */
    public static void main(String[] args) {
        Point a = new Point(0, 0);
        Point b = new Point(2, 0);
        Point c = new Point(3.68, 1.08);
        Point d = new Point(4.51, 2.9);
        Point e = new Point(4.23, 4.88);
        /*Point f = new Point(2.92, 6.39);
        Point g = new Point(1, 6.96);
        Point h = new Point(-0.92, 6.39);
        Point i = new Point(-2.23, 4.88);
        Point j = new Point(-2.51, 2.9);
        Point k = new Point(-1.68, 1.08);*/


        LinkedList<Point> listePoints = new LinkedList<Point>();
        listePoints.add(a);
        listePoints.add(b);
        listePoints.add(c);
        listePoints.add(d);
        listePoints.add(e);
        /*listePoints.add(f);
        listePoints.add(g);
        listePoints.add(h);
        listePoints.add(i);
        listePoints.add(j);*/

        Polygone polygone = new Polygone(listePoints);

        Triangulation maTriangulation = new Triangulation(polygone);
        long startTime = System.nanoTime();
        //Triangulation maTriangulationMinimale = essaisSuccessifs(maTriangulation);
        Triangulation maTriangulationMinimale2 = essaisSuccessifsElagage(maTriangulation, Double.POSITIVE_INFINITY);
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("Execution time in milliseconds: " + timeElapsed);
        //System.out.println("Ma triangulation minimale est :" + maTriangulationMinimale.toString());
        System.out.println("Ma triangulation minimale est :" + maTriangulationMinimale2.toString());

    }

}

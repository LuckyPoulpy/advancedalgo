import java.util.LinkedList;

public abstract class AppliProgDynamique {
    /**
     * Mise en place de l'algo par programmation dynamique
     * @param p un polygone avec n sommets, n>=3
     * @return
     */
    public static TriangulationDynamique progDynamique(Polygone p)
    {
        TriangulationDynamique triangulation = new TriangulationDynamique(p);
        triangulation.fillMatrice();
        try {
            triangulation.genListeCordesUtil(1, p.nombreDeSommets());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return triangulation;
    }

    /**
     * Permet de tester l'algorithme par programmation dynamique
     * @param args
     */
    public static void main(String[] args) {
        Point a = new Point(1,0, 0);
        Point b = new Point(2, 2, 0);
        Point c = new Point(3, 3.68, 1.08);
        Point d = new Point(4, 4.51, 2.9);
        Point e = new Point(5, 4.23, 4.88);
        //Point f = new Point(6, 2.92, 6.39);

        LinkedList<Point> listePoints = new LinkedList<Point>();
        listePoints.add(a);
        listePoints.add(b);
        listePoints.add(c);
        listePoints.add(d);
        listePoints.add(e);
        //listePoints.add(f);

        Polygone polygone = new Polygone(listePoints);

        TriangulationDynamique resultatStpFonctionne = progDynamique(polygone);
    }
}

import java.io.IOException;
import java.util.LinkedList;

public class AppliAlgoGlouton {
    /**
     * Applique l'algo glouton sur le polygone p
     *
     * @param p Polygone voulu
     * @return Triangulation contenant la solution
     */
    public static Triangulation algoGlouton(Polygone p) {
        TriangulationGlouton tGlouton = new TriangulationGlouton(p);
        Corde cordeExterieureMinimale;
        try {
//			System.err.println("Nb sommets restants : "+res.getNbSommetsRestants());
            while (tGlouton.getNbSommetsRestants() > 3) {
                cordeExterieureMinimale = null;
                if (tGlouton.getCordesExterieures().isEmpty()) System.err.println("Aucune corde exterieure trouvée");
                for (Corde c : tGlouton.getCordesExterieures()) {
                    if (cordeExterieureMinimale == null || cordeExterieureMinimale.getLength() > c.getLength()) {
                        cordeExterieureMinimale = c;
                    }
                }

                try {
                    tGlouton.ajouterCorde(cordeExterieureMinimale);
                } catch (IOException e) {
                    e.printStackTrace();
                }
//				System.err.println("Nb sommets restants : "+res.getNbSommetsRestants());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tGlouton;
    }

    /**
     * Permet de tester l'algo glouton
     * @param args
     */
    public static void main(String[] args) {

        Point a = new Point(1,0, 0);
        Point b = new Point(2,2, 0);
        Point c = new Point(3,3.68, 1.08);
        Point d = new Point(4,4.51, 2.9);
        Point e = new Point(5,4.23, 4.88);
        Point f = new Point(6,2.92, 6.39);
        Point g = new Point(7,1, 6.96);
        Point h = new Point(8,-0.92, 6.39);
        Point i = new Point(9,-2.23, 4.88);
        Point j = new Point(10,-2.51, 2.9);
        Point k = new Point(11,-1.68, 1.08);

        LinkedList<Point> listePoints = new LinkedList<Point>();
        listePoints.add(a);
        listePoints.add(b);
        listePoints.add(c);
        listePoints.add(d);
        listePoints.add(e);
        listePoints.add(f);
        listePoints.add(g);
        listePoints.add(h);
        listePoints.add(i);
        listePoints.add(j);

        Polygone polygone = new Polygone(listePoints);

        Triangulation maTriangulationMinimale = algoGlouton(polygone);
    }
}

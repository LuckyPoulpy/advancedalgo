import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Triangulation {

    private Polygone polygone;
    private List<Corde> listeCordesUtilisees;

    /**
     * Constructeur de la classe Triangulation
     * @param polygone un polygone
     */
    public Triangulation(Polygone polygone){
        this.polygone = polygone;
        this.listeCordesUtilisees = new LinkedList<Corde>();
    }

    /* Getters et Setters de polygone et listeCordesUtilisees */
    public Polygone getPolygone() {
        return polygone;
    }

    public void setPolygone(Polygone polygone) {
        this.polygone = polygone;
    }

    public List<Corde> getListeCordesUtilisees() {
        return listeCordesUtilisees;
    }

    public void setListeCordesUtilisees(List<Corde> listeCordesUtilisees) {
        this.listeCordesUtilisees = listeCordesUtilisees;
    }

    public List<Corde> getCordesPossibles(){
        return this.polygone.getListeCordesPossibles();
    }

    /**
     * Vérifie si la triangulation est valide en vérifiant que le nombre de cordes utilisees est égal au nombre de sommets du polygone moins 3
     * @return un booleen, la validite de la triangulation
     */
    public boolean estValide(){
        return (listeCordesUtilisees.size() == this.polygone.nombreDeSommets()-3);
    }

    /**
     * Calcule la longueur de la triangulation en additionnant la longueur de toutes les cordes
     * @return un double, la longueur
     */
    public double longueur(){
        double longueurTriangulation = 0;
        for(Corde corde: listeCordesUtilisees){
            longueurTriangulation += corde.getLength();
        }
        return longueurTriangulation;
    }

    /**
     * Prend une corde en parametre et verifie si cette corde est valide ou non (pas d'intersection ni de doublon avec la liste des cordes deja utilisees)
     * @param cordeATracer
     * @return un booleen, la validite de la corde
     */
    public boolean valideCorde(Corde cordeATracer){
        int cpt = 0;
        /*On vérifie si cette corde n'existe pas en parcourant la liste des cordes tracees et on vérifie si celle-ci croise aucune autre corde*/
        while(cpt < this.listeCordesUtilisees.size() && !(cordeATracer.equals(this.listeCordesUtilisees.get(cpt))) && !(cordeATracer.intersection(this.listeCordesUtilisees.get(cpt))) && !(this.polygone.getListeCotes().contains(cordeATracer))) {
            /*System.out.println("Corde à tester : "+ cordeATracer.toString() + ", egalitié avec la corde "+this.listeCordesUtilisees.get(cpt).toString()+" : "+ cordeATracer.equals(this.listeCordesUtilisees.get(cpt))
                    + ", intersection avec la corde : " +cordeATracer.intersection(this.listeCordesUtilisees.get(cpt)));*/
            cpt++;
        }
        /*Si la corde a passé tous les tests elle est valide et peut etre tracee*/
        if(cpt == this.listeCordesUtilisees.size()){
            return true;
        }
        return false;
}

    /**
     * Ajoute une corde a la liste des cordes utilisees (verifie d'abord si celle-ci est valide)
     * @param corde
     * @throws Exception
     */
    public void ajouterCorde(Corde corde) throws Exception{
        if(!valideCorde(corde)){
            throw new Exception("Corde invalide");
        }
        this.listeCordesUtilisees.add(corde);
    }

    public String toString(){
        String listeDePoints;

        return("Triangulation : nombre de cordes : "+this.getListeCordesUtilisees().size()+" et longueur : "+longueur());
    }

    /**
     * @return un clone partiel de la triangulation
     */
    public Triangulation creer_deep_copy(){	// Renvoie une deep copy de la
        Triangulation res = new Triangulation(this.polygone);
        List<Corde> cordesUtilisees_DeepCopy = new LinkedList<Corde>(); // DeepCopy
        for(Corde c : this.listeCordesUtilisees){
            cordesUtilisees_DeepCopy.add(c);
        }
        res.setListeCordesUtilisees(cordesUtilisees_DeepCopy);
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangulation that = (Triangulation) o;
        List<Corde> listeCordesUtiliseesInversee = new LinkedList<Corde>();
        for(int i = listeCordesUtilisees.size() - 1; i >= 0; i--){
            listeCordesUtiliseesInversee.add(listeCordesUtilisees.get(i));
        }
        return Objects.equals(polygone, that.polygone) &&
                (Objects.equals(listeCordesUtilisees, that.listeCordesUtilisees) || Objects.equals(listeCordesUtiliseesInversee, that.listeCordesUtilisees)) && (that.longueur() == this.longueur());
    }

    @Override
    public int hashCode() {
        return Objects.hash(polygone, listeCordesUtilisees);
    }

    public static void main(String[] args){
        Point a = new Point(0,0);
        Point b = new Point(1,0);
        Point c = new Point(1,1);
        Point d = new Point(0,1);
        Point e = new Point(-0.5,0.5);

        LinkedList<Point> listePoints = new LinkedList<Point>();
        listePoints.add(a);
        listePoints.add(b);
        listePoints.add(c);
        listePoints.add(d);
        listePoints.add(e);

        Polygone polygone = new Polygone(listePoints);

        Triangulation maTriangulation1 = new Triangulation(polygone);
        Triangulation maTriangulation2 = new Triangulation(polygone);
        try {
            maTriangulation1.ajouterCorde(new Corde(a,c));
            maTriangulation1.ajouterCorde(new Corde(c,e));
            maTriangulation2.ajouterCorde(new Corde(c,e));
            maTriangulation2.ajouterCorde(new Corde(a,c));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //System.out.println(maTriangulation1.equals(maTriangulation2));
        try {
            maTriangulation1.ajouterCorde(new Corde(b ,a));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void trierCordes(){
        return;
    }
}

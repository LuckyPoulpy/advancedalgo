import java.util.LinkedList;
import java.util.Scanner;

public class Main {

    /**
     * Permet de générer un polygone aléatoire inscrit dans une ellipse
     * @param nbSommets le nombre de sommets souhaités pour le polygone
     * @param exc l'excentrité de l'ellipse
     * @return un polygone
     */
    public static Polygone genererPolygone(int nbSommets, double exc){
        LinkedList<Point> listePointsPolygoneGenere = new LinkedList<Point>();

        double plageDePoints = (360d/nbSommets * (Math.PI/180));
        int i;
        for(i=0; i<nbSommets; i++){
            double[] coordonnees = new double[2];
            coordonnees[1] = (0.9*plageDePoints * Math.random())+(i+0.05)*plageDePoints;
            coordonnees[0] = 1/Math.sqrt(1-exc*Math.cos(coordonnees[1])*Math.cos(coordonnees[1]));
            coordonnees = polaireACartesien(coordonnees);
            Point nouveauPoint = new Point(i+1, coordonnees[0], coordonnees[1]);
            listePointsPolygoneGenere.add(nouveauPoint);
        }
        Polygone p = new Polygone(listePointsPolygoneGenere);

        return p;
    }

    /**
     * Convertit des coordonnées polaires en coordonnées cartésiennes
     * @param polaire un tableau à deux entrées comportant les coordonnées polaires r et theta
     * @return un tableau de deux coordonnées cartésiennes
     */
    private static double[] polaireACartesien(double[] polaire){
        double[] cartesiennes = new double[2];
        cartesiennes[0] = polaire[0] * Math.cos(polaire[1]);
        cartesiennes[1] = polaire[0] * Math.sin(polaire[1]);
        return cartesiennes;
    }

    public static void main(String[] args){
        int nbDeSommets = 0;
        double courbureEllipse = 0.01;

        //On demande le nombre de sommets souhaités par l'utilisateur
        Scanner text = new Scanner(System.in);
        System.out.println("Entrer un nombre de sommets (>3) : ");
        String inNbSommets = text.nextLine();
        //Avec une erreur si le nombre rentré est incorrect
        while(Integer.parseInt(inNbSommets) <= 3){

            System.out.println("Veuillez rentrer un nombre de sommets supérieur à 3 ");
            inNbSommets = text.nextLine();

        }
        nbDeSommets = Integer.parseInt(inNbSommets);

        //On demande ensuite l'excentricité de l'ellipse dans laquelle sera inscrit le polygone
        System.out.println("Entrer une excentricité de l'ellipse dans laquelle le polygone sera inscrit (entre 0 (cercle) et 0.99) : ");
        String inCourbure = text.nextLine();
        while(Double.parseDouble(inCourbure) < 0 || Double.parseDouble(inCourbure) > 0.99){
            System.out.println("Excentricité de l'ellispe incorrecte, veuillez rentrer une valeur entre 0 et 0.99");
            inCourbure = text.nextLine();
        }

        double courbure = Double.parseDouble(inCourbure);
        
        Polygone monPolygone = genererPolygone(nbDeSommets, courbure);

        //Et on demande quel algorithme l'utilisateur souhaite utiliser
        System.out.println("Quel algorithme souhaitez-vous utiliser pour trianguler ce polygone ?");
        System.out.println("Essais successifs sans elagage ? (\"esse\"), essais successifs avec elagage (\"esae\"), " +
                "programmation dynamique (\"progd\") ou algorithme glouton (\"ag\")");

        String algo = text.nextLine();

        while(!algo.equals("esse") && !algo.equals("esae") && !algo.equals("progd") && !algo.equals("ag")){
            System.out.println("Erreur : entrer un code d'algo correct : esse, esae, progd ou ag");
            algo = text.nextLine();
        }
        System.out.println("Algo choisi : "+algo);
        //Et on calcule la triangulation

        switch (algo){
            case "esse":
                Triangulation maTriangulation = new Triangulation(monPolygone);
                long startTime = System.nanoTime();
                Triangulation maTriangulationMinimale = AppliEssaisSuccessifs.essaisSuccessifs(maTriangulation);
                long endTime = System.nanoTime();
                long timeElapsed = endTime - startTime;
                System.out.println(maTriangulationMinimale.toString());
                System.out.println("Temps de calcul en milliseconds : " + timeElapsed / 1000000);
                break;
            case "esae":
                Triangulation maTriangulation1 = new Triangulation(monPolygone);
                long startTime1 = System.nanoTime();
                Triangulation maTriangulationMinimale1 = AppliEssaisSuccessifs.essaisSuccessifsElagage(maTriangulation1,Double.POSITIVE_INFINITY);
                long endTime1 = System.nanoTime();
                long timeElapsed1 = endTime1 - startTime1;
                System.out.println(maTriangulationMinimale1.toString());
                System.out.println("Temps de calcul en milliseconds : " + timeElapsed1 / 1000000);
                break;
            case "progd":
                long startTime2 = System.nanoTime();
                TriangulationDynamique maTriangulationMinimale3 = AppliProgDynamique.progDynamique(monPolygone);
                long endTime2 = System.nanoTime();
                long timeElapsed2 = endTime2 - startTime2;
                System.out.println(maTriangulationMinimale3.toString());
                System.out.println("Temps de calcul en milliseconds : " + timeElapsed2 / 1000000);
                break;
            case "ag":
                long startTime3 = System.nanoTime();
                Triangulation maTriangulationMinimale4 = AppliAlgoGlouton.algoGlouton(monPolygone);
                long endTime3 = System.nanoTime();
                long timeElapsed3 = endTime3 - startTime3;
                System.out.println(maTriangulationMinimale4.toString());
                System.out.println("Temps de calcul en milliseconds : " + timeElapsed3 / 1000000);
                break;
            default:
                System.out.println("ERROR ERROR EXTERMINATE");
        }
    }
}

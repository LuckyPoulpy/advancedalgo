public class TriangulationDynamique extends Triangulation {
    private Double[][] matDistances = null;
    private MatriceDynamique matEtats = null;

    public Double[][] getMatDistances() {
        return matDistances;
    }

    public double getDistance(int i, int j)
    {
        return getMatDistances()[i-1][j-1];
    }

    public void setMatDistances(Double[][] matDistances) {
        this.matDistances = matDistances;
    }

    /**
     * Constructeur de la classe TriangulationDynamique, qui hérite de la classe Triangulation
     * @param polygone un polygone de taille >= 3
     */
    public TriangulationDynamique(Polygone polygone) {
        super(polygone);
        int nbSommets = polygone.getListePoints().size();
        this.matDistances = new Double[nbSommets][nbSommets];
        for(Corde c: this.getCordesPossibles())
        {
            int A = c.getSommetA().getNoPt()-1;
            int B = c.getSommetB().getNoPt()-1;
            this.matDistances[A][B] = c.getLength();
            this.matDistances[B][A] = c.getLength();
        }
        this.matEtats = new MatriceDynamique(polygone);
    }

    /**
     * Remplit chaque état de la matrice d'états de la triangulation
     */
    public void fillMatrice()
    {
        int i, t;

        int nbSommets = this.getPolygone().nombreDeSommets();

        for(t = 4; t <= nbSommets; t++)
        {
            for(i=1; i<= nbSommets-t+1;i++)
            {
                defEtat(i,t);
            }
        }
    }

    /**
     * Définit l'état (i, t) de la triangulation actuelle
     * @param i le numéro du point courant
     * @param t la taille du polygone étudié
     */
    private void defEtat(int i, int t)
    {
        Etat e = new Etat(Double.POSITIVE_INFINITY, null);
        int nbSommets = this.getPolygone().nombreDeSommets();
        if(e.getDistance() > matEtats.getEtat(i, t-1).getDistance() + getDistance(i, i+t-2))
        {
            e.setDistance(matEtats.getEtat(i, t-1).getDistance() + getDistance(i, i+t-2));
            e.setPointCourant(this.getPolygone().getListePoints().get(i+t-3));
        }

        if(e.getDistance() > matEtats.getEtat(i+1, t-1).getDistance() + getDistance(i+1, i+t-1))
        {
            e.setDistance(matEtats.getEtat(i+1, t-1).getDistance() + getDistance(i+1, i+t-1));
            e.setPointCourant(this.getPolygone().getListePoints().get(i));
        }

        for(int k = 2 ; k < t-2 ; k++)
        {
            if(i+k < nbSommets &&
                    (t-k < nbSommets) &&
                    e.getDistance() > matEtats.getEtat(i, k+1).getDistance() + matEtats.getEtat(i+k,t-k).getDistance() + getDistance(i, i+k) + getDistance(i+k, i+t-1))
            {
                e.setDistance(matEtats.getEtat(i, k+1).getDistance() + matEtats.getEtat(i+k, t-k).getDistance() + getDistance(i, i+k) + getDistance(i+k, i+t-1));
                e.setPointCourant(this.getPolygone().getListePoints().get(i+k-1));
            }
        }
        matEtats.setEtat(e, i, t);
    }

    /**
     * Génère la liste des cordes utilisées pour parvenir au résultat au problème T(i,t)
     * @param i le numéro du point
     * @param t la taille du polynome
     * @throws Exception
     */
    public void genListeCordesUtil(int i, int t) throws Exception {

        Point p = this.matEtats.getEtat(i, t).getPointCourant();

        if(p == null){
            // Si on arrive a un endroit ou on a pas choisi de point bah lolu
            if(t>3)
                System.err.println("Tentative de lecture d'un état résolu mais son point n'est pas défini alors que ce n'est pas un triangle");
        }
        else if(p.getNoPt() == i+1)
        {
            Corde c = new Corde(this.getPolygone().getListePoints().get(i), this.getPolygone().getListePoints().get(i+t-2));
            this.ajouterCorde(c);
            genListeCordesUtil(i+1, t-1);
        }
        else if(p.getNoPt() == i+t-2)
        {
            Corde c = new Corde(this.getPolygone().getListePoints().get(i-1), this.getPolygone().getListePoints().get(i+t-3));
            this.ajouterCorde(c);
            genListeCordesUtil(i, t-1);
        }
        else
        {
            Corde c = new Corde(this.getPolygone().getListePoints().get(i-1), this.getPolygone().getListePoints().get(p.getNoPt()-1));
            this.ajouterCorde(c);

            genListeCordesUtil(i, p.getNoPt()-i+1);

            c = new Corde(this.getPolygone().getListePoints().get(p.getNoPt()-1), this.getPolygone().getListePoints().get(i+t-2));
            this.ajouterCorde(c);
            genListeCordesUtil(p.getNoPt(), t-p.getNoPt()+i);
        }
    }

    /**
     * Getter de la matrice des états du polynome
     * @return matEtats du type MatriceDynamique
     */
    public MatriceDynamique getMatEtats() {
        return matEtats;
    }

    /**
     * Setter de matEtats
     * @param matEtats la nouvelle valeur de la matrice des états
     */
    public void setMatEtats(MatriceDynamique matEtats) {
        this.matEtats = matEtats;
    }
}

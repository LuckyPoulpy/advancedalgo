public class Etat {

    private double distance;
    private Point pointCourant;

    /**
     * Constructeur pour la classe Etat
     * @param val la valeur de la distance de l'état
     * @param p un polygone
     */
    public Etat(double val, Point p)
    {
        this.distance = val;
        this.pointCourant = p;
    }

    /**
     * Getter pour l'attribut distance
     * @return un double
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Setter pour l'attribut Distance
     * @param distance un double nouvelle valeur de distance
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }

    /**
     * Getter pour l'attribut pointCourant
     * @return un Point
     */
    public Point getPointCourant() {
        return pointCourant;
    }

    /**
     * Setter pour l'attribut pointCourant
     * @param pointCourant un Point
     */
    public void setPointCourant(Point pointCourant) {
        this.pointCourant = pointCourant;
    }

    /**
     * Affiche les information sur l'Etat
     * @return un String contenant les valeurs des attributs de l'etat
     */
    @Override
    public String toString()
    {
        return "Etat : distance = "+this.distance+", point courant : "+this.pointCourant;
    }

}

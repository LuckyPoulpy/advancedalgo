public class Corde {
    private Point sommetA;
    private Point sommetB;
    private boolean corde;
    private boolean trace;

    /**
     * Constucteur de la classe Corde
     * @param a le sommet A du la corde
     * @param b le sommet B de la corde
     */
    public Corde(Point a, Point b) {
        this.sommetA = a;
        this.sommetB = b;
    }

    /**
     * Retourne le sommet A de la corde
     * @return un point
     */
    public Point getSommetA() {
        return sommetA;
    }

    /**
     * Paramètre le sommet A
     * @param sommetA un point
     */
    public void setSommetA(Point sommetA) {
        this.sommetA = sommetA;
    }

    /**
     * Retourne le sommet B de la corde
     * @return un point
     */
    public Point getSommetB() {
        return sommetB;
    }

    /**
     * Paramètre le sommet B de la corde
     * @param sommetB un point
     */
    public void setSommetB(Point sommetB) {
        this.sommetB = sommetB;
    }

    /**
     * Retourne la longueur de la corde
     * @return un double, la longueur
     */
    public double getLength() {
        return Math.sqrt(Math.pow((sommetB.getx() - sommetA.getx()), 2) + Math.pow((sommetB.gety() - sommetA.gety()), 2));
    }

    /**
     * Vérifie si une corde est égale à une autre, en prenant soin de vérifier le sens de la corde (la corde AB est égale à la corde BA)
     * @param obj
     * @return un booleen
     */
    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof Corde)) {
            return false;
        }
        Corde s = (Corde)obj;
        return (
                /*S'ils ont les mêmes points et que ce sont tous les deux des cordes, ils sont égaux*/
                (this.getSommetA().equals(s.getSommetA()) && this.sommetB.equals(s.getSommetB()))
                || (this.getSommetA().equals(s.getSommetB()) && this.getSommetB().equals(s.getSommetA()))
        );
    }

    /**
     * Vérifie si une corde est en intersection avec une autre
     * @param s1 une corde
     * @return un booleen
     */
    public boolean intersection(Corde s1) {
        /* On vérifie si les deux cordes n'ont pas un sommet commun, si c'est le cas, on ignore ce type d'intersection */
        if(this.sommetB.equals(s1.getSommetB()) || this.sommetB.equals(s1.getSommetA()) || this.sommetA.equals(s1.getSommetA()) || this.sommetA.equals(s1.getSommetB())){
            return false;
        }

        /*On vérifie d'abord si une des cordes n'est pas verticale
        * Si tel est le cas, on teste si celle-ci coupe l'autre corde*/
        if(((this.sommetB.getx() - this.sommetA.getx()) == 0) && ((s1.sommetB.getx() - s1.sommetA.getx()) != 0)){
            if(((this.sommetB.getx() < s1.sommetB.getx()) && this.sommetB.getx() > s1.sommetA.getx())
                    || ((this.sommetB.getx() < s1.sommetA.getx()) && this.sommetB.getx() > s1.sommetB.getx())){
                return true;
            }
            return false;
        }
        else if(((this.sommetB.getx() - this.sommetA.getx()) != 0) && ((s1.sommetB.getx() - s1.sommetA.getx()) == 0)){
            if(((s1.sommetB.getx() < this.sommetB.getx()) && s1.sommetB.getx() > this.sommetA.getx())
                    || ((s1.sommetB.getx() < this.sommetA.getx()) && s1.sommetB.getx() > this.sommetB.getx())){
                return true;
            }
            return false;
        }
        /*Si les deux cordes sont verticales elles sont parallèles et donc pas en intersection*/
        else if(((this.sommetB.getx() - this.sommetA.getx()) == 0) && ((s1.sommetB.getx() - s1.sommetA.getx()) == 0)){
            return false;
        }
        else{
            /* Calcul des coefficients directeurs et des ordonnées à l'origine des segments */
            double coeffDirecteur1 = (this.sommetB.gety() - this.sommetA.gety()) / (this.sommetB.getx() - this.sommetA.getx());
            double coeffDirecteur2 = (s1.sommetB.gety() - s1.sommetA.gety()) / (s1.sommetB.getx() - s1.sommetA.getx());
            double ordonneeAlOrigine1 = this.sommetB.gety() - coeffDirecteur1 * this.sommetB.getx();
            double ordonneeAlOrigine2 = s1.sommetB.gety() - coeffDirecteur2 * s1.sommetB.getx();
            /*Si les deux segments ont le même coefficient directeur, alors ils sont parallèles -> pas d'intersection*/
            if (coeffDirecteur1 == coeffDirecteur2) {
                return false;
            } else {
                /*On calcule le point d'intersection des deux droites*/
                double x = (ordonneeAlOrigine2 - ordonneeAlOrigine1) / (coeffDirecteur1 - coeffDirecteur2);
                /*Si ce point a bien son abscisse comprises entre les abscisses des sommets des deux segments, alors il y a une intersection*/
                if ((this.sommetA.getx() <= x && x <= this.sommetB.getx() || (this.sommetB.getx() <= x && x <= this.sommetA.getx()))) {
                    if ((s1.sommetA.getx() <= x && x <= s1.sommetB.getx() || (s1.sommetB.getx() <= x && x <= s1.sommetA.getx()))) {
                        return true;
                    }
                    return false;
                }
                return false;
            }
        }
    }

    /**
     * Affiche les informations sur la corde
     * @return une chaine de caractères
     */
    public String toString(){
        return("Corde de sommet A : (" + sommetA.getx() + ";" + sommetA.gety() + ") et de sommet B : (" + sommetB.getx() + ";" + sommetB.gety() + ")");
    }
    public static void main(String[] args){
        Point a = new Point(0,0);
        Point b = new Point(1,0);
        Point c = new Point(1,1);
        Point d = new Point(0,1);
        Point e = new Point(-0.5,0.5);
        Point f = new Point(0.5,-0.5);
        Corde c1 = new Corde(a,d);
        Corde c2 = new Corde(b,e);
        Corde c3 = new Corde(a,b);
        Corde c4 = new Corde(c,f);
        System.out.println(c1.intersection(c2));
        System.out.println(c3.intersection(c4));
    }
}

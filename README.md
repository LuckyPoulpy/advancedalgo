# Projet d'Algorithmique avancée - Cléis Kounalis et Marie Peter - Avril 2020

## Prérequis :
Un ordinateur sous Windows ou Linux
Le code source disponible sur https://gitlab.com/LuckyPoulpy/advancedalgo.git

## Lancement du projet :
* Sur Windows ou Linux: 
  - Ouvrir une invite de commande 
  - Se rendre dans le dossier à la racine du projet
  - lancer la commande ```java -jar ./out/artifacts/advancedalgo_jar/advancedalgo.jar```


